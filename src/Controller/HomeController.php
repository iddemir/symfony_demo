<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends Controller
{
    /**
     * @Route("/")
     */
    public function Index()
    {
        $number = mt_rand(0, 100);

        return $this->render('UserInterface/MasterPage/app.master.twig', array(
            'number' => $number,
        ));
    }
}